(require 'prelude-company)
(require 'prelude-evil)
(require 'prelude-ido) ;; Super charges Emacs completion for C-x C-f and more

;; Programming languages support
(require 'prelude-clojure)
(require 'prelude-erlang)
(require 'prelude-elixir)
(require 'prelude-js)
(require 'prelude-scss)

;; Major modes
(require 'markdown-mode)
(require 'vue-mode)

;; Neotree
(require 'neotree)
(global-set-key [f8] 'neotree-toggle)
